from numpy import genfromtxt
import numpy as np
import matplotlib.pyplot as plt

data = genfromtxt('builds/50.csv', delimiter=',')

plt.imshow(data, cmap='magma_r', interpolation='bicubic')
# plt.xlim(left=5)

plt.axes().get_xaxis().set_ticks([])
plt.axes().get_yaxis().set_ticks([])

plt.show()

# print(my_data)
