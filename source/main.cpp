#include <iostream>
#include "../include/Timer.h"
#include "../include/ResourceMgr.h"
#include "../include/OutputMgr.h"
#include "../include/BBSolver.h"
#include "../include/TSSolver.h"
#include "../include/GASolver.h"
#include "../include/DPSolver.h"
#include <string>
#include <vector>
#include <filesystem>
#include <cmath>
#include "time.h"

// #include <iomanip> // std::setprecision

using namespace std;
namespace fs = std::filesystem;

arrType current_graph;
string current_graph_name = "null";
vector<string> available_files;

Timer timer;
DPSolver dp;
BBSolver bb;
TSSolver ts;
GASolver ga;

int time_micros;

void get_available_files()
{
    available_files.clear();

    std::string path = "./resources";
    for (const auto &entry : fs::directory_iterator(path))
        available_files.push_back(entry.path());
}

void print_avialable_files()
{
    for (int i = 0; i < available_files.size(); i++)
    {
        cout << i << ": " << available_files[i] << endl;
    }
}

void display_menu()
{
    cout << "==========================" << endl;
    cout << "Obecnie załadowany graf: " << current_graph_name << endl;
    cout << "==========================" << endl;
    cout << "w: Wczytaj graf z pliku" << endl;
    cout << "g: Wygeneruj graf losowo" << endl;
    cout << "p: Pokaż graf" << endl;
    cout << "x: Rozwiąż TSP za pomocą algorytmu genetycznego" << endl;
    cout << "t: Rozwiąż TSP za pomocą Tabu Search" << endl;
    cout << "d: Rozwiąż TSP za pomocą Dynamic Programming" << endl;
    cout << "b: Rozwiąż TSP za pomocą Branch & Bound" << endl;
    cout << "e: Wyjdź" << endl;

    cout << "==========================" << endl;
}

char get_choice()
{
    char c;
    cin >> c;

    return c;
}

void handle_choice(char c)
{

    switch (c)
    {

    case 'w':
        get_available_files();
        print_avialable_files();

        cout << "numer pliku: ";
        int choice;
        cin >> choice;

        current_graph_name = available_files[choice];
        current_graph = ResourceMgr::readGraphFromFile(available_files[choice]);
        break;

    case 'g':
        int size;
        cout << "Podaj liczbę miast: ";
        cin >> size;

        current_graph_name = "generated_" + to_string(size);
        current_graph = ResourceMgr::generateRandomGraph(size);
        break;

    case 'p':
        OutputMgr::showGraph(current_graph);
        break;

    case 'e':
        exit(0);
        break;

    case 't':

        cout << "Podaj czas pracy w mikrosekundach: ";
        cin >> time_micros;

        ts.max_time_microseconds = time_micros;

        timer.start();
        ts.solve(current_graph);
        timer.stop();
        ts.display_result();
        timer.printTimeInSeconds();
        break;

    case 'x':

        cout << "Podaj czas pracy w mikrosekundach: ";
        cin >> ga.max_time_microseconds;

        cout << "Podaj rozmiar populacji: ";
        cin >> ga.population_size;

        cout << "Podaj procentową szansę mutacji [0-100]: ";
        cin >> ga.mutation_chance;

        timer.start();
        ga.solve(current_graph);
        timer.stop();
        ga.display_result();
        timer.printTimeInSeconds();
        break;

    case 'd':
        timer.start();
        dp.solve(current_graph);
        timer.stop();
        dp.display_result();
        timer.printTimeInSeconds();
        break;

    case 'b':
        timer.start();
        bb.solve(current_graph);
        timer.stop();
        bb.display_result();
        timer.printTimeInSeconds();
        break;

    default:
        break;
    }
}

void menu_loop()
{
    while (!0)
    {
        display_menu();
        char c = get_choice();
        handle_choice(c);
    }
}

int main(int argc, char **argv)
{
    srand(time(NULL));

    ga.population_size = 10;
    ga.mutation_chance = 10;

    // If no arguments given, start in interactive mode
    if (argc == 1)
    {
        menu_loop();

        return 0;
    }

    /*
    int min = 0;
    int max = 10;

    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(min, max);

    for (int i = 0; i < 100; i++)
    {

        cout << distr(generator) << endl;
        
    }
    */

   GASolver s;
   s.mutation_chance = 10;
   s.population_size = 100;
   auto graph = ResourceMgr::readGraphFromFile("resources/tsp_17.txt");
   s.max_time_microseconds = 10000000;
   
   s.solve(graph);

   /*
    int problem_size = 200;

    for (int population_size = 20; population_size <= 100; population_size += 20)
    {
        float sum = 0;
        for (int i = 0; i < 5; i++)
        {
            auto graph = ResourceMgr::generateRandomGraph(problem_size);

            GASolver s;
            s.mutation_chance = 10;
            s.population_size = population_size;
            s.max_time_microseconds = 10000000;

            s.solve(graph);

            sum += s.min_cost;
        }

        cout << sum/5.0 << ",";
    }
    */

    /*
    GASpecimen a(9);
    GASpecimen b(9);

    a.genes.push_back(0);
    a.genes.push_back(2);
    a.genes.push_back(3);
    a.genes.push_back(4);
    a.genes.push_back(5);
    a.genes.push_back(6);
    a.genes.push_back(7);
    a.genes.push_back(8);
    a.genes.push_back(0);

    b.genes.push_back(0);
    b.genes.push_back(8);
    b.genes.push_back(7);
    b.genes.push_back(6);
    b.genes.push_back(5);
    b.genes.push_back(4);
    b.genes.push_back(3);
    b.genes.push_back(2);
    b.genes.push_back(0);

    a.print();
    cout << endl;
    b.print();
    cout << endl;

    GASpecimen c = GASpecimen::breed_from(&a, &b);
    c.print();
    */
}
