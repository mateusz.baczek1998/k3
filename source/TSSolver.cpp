#include "../include/TSSolver.h"

using namespace std;

/*
sBest ← s0

bestCandidate ← s0

tabuList ← []

tabuList.push(s0)

while (not stoppingCondition())

    sNeighborhood ← getNeighbors(bestCandidate)

    for (sCandidate in sNeighborhood)

        if ( (not tabuList.contains(sCandidate)) and (fitness(sCandidate) > fitness(bestCandidate)) )

            bestCandidate ← sCandidate

        end

    end

    if (fitness(bestCandidate) > fitness(sBest))

        sBest ← bestCandidate

    end

    tabuList.push(bestCandidate)

    if (tabuList.size > maxTabuSize)

        tabuList.removeFirst()

    end

end

return sBest


*/

int TSSolver::calculate_cost(vector<int> &path)
{

    // print(permutations);

    int cost = 0;

    int for_limit = path.size() - 1;

    for (int i = 0; i < for_limit; i++)
    {
        // cout << "from " << i << " to " << i+1 << endl;
        cost += this->map[path[i]][path[i + 1]];
    }

    return cost;
}

bool TSSolver::stoppingCondition()
{
    this->timer.stop();
    return timer.getTimeInMicroSec() > this->max_time_microseconds;
}

bool TSSolver::onTabuList(int i, int j) {
    for(auto tabu: this->tabuList)
        if(tabu.match(i,j))
            return true;

    return false;
}

vector<TabuSolution> TSSolver::get_neighbours(vector<int> &path)
{
    vector<TabuSolution> neighbours;

    for (int i = 1; i < map.size() - 1; i++)
    {
        for(int j = i; j < map.size() - 1; j++) {
            if(j == i)
                continue;

            neighbours.push_back(TabuSolution(path, i, j));
        }
    }

    // cout << "worked, somehow" << endl;

    return neighbours;
}

void TSSolver::solve(arrType map)
{

    this->map = map;

    this->best_path.clear();

    this->timer.start();

    for (int i = 0; i < map.size(); i++)
    {
        this->best_path.push_back(i);
    }


    // Globally best solution
    this->best_path.push_back(0);
    this->min_cost = calculate_cost(best_path);
    

    TabuSolution current_best_path = TabuSolution(this->best_path, 0,0);
    int current_best_cost = this->min_cost;

    int iters_without_update = 0;

    while (!stoppingCondition())
    {
        iters_without_update++;
        
        // cout << "Getting neighbours... ";
        auto neighbours = get_neighbours(current_best_path.solution);
        // cout << "[ OK ]" << endl;

        current_best_path = neighbours[0];

        // cout << "Cost calc... ";
        int best_cost_in_neighbourhood = calculate_cost(current_best_path.solution);
        // cout << "[ OK ]" << endl;

        // cout << "The list... ";
        for (auto neighbour : neighbours)
        {
            int cost = calculate_cost(neighbour.solution);


            // Kryterium aspiracji - globalnie najlepsze rozwiązanie
            if (cost < min_cost) {

                best_path = neighbour.solution;
                min_cost = cost;


                current_best_path = neighbour;
                best_cost_in_neighbourhood = cost;
                // cout << "YEEEAAAHH" << endl;
                //cout << min_cost << endl;
                //cout << "iters wo update" << iters_without_update << endl;
                iters_without_update = 0;
                continue;
            }

            if(onTabuList(neighbour.tabuElement.i,neighbour.tabuElement.j)) {
                // cout << "Bruh, thats a taboo.." << endl;
                continue;
            }

            if (cost < best_cost_in_neighbourhood) {
                current_best_path = neighbour;
                best_cost_in_neighbourhood = cost;
            
            }            
        }

        // cout << "move " << current_best_path.tabuElement.i << " " << current_best_path.tabuElement.j << endl;
        // cout << "[ OK ]" << endl;

        // cout << "Tabu list pop... ";
        this->tabuList.push_back(current_best_path.tabuElement);


        while (this->tabuList.size() > this->map.size()/2) {
            // cout << "POPPIN";
            this->tabuList.pop_front();
        }
        // cout << "[ OK ]" << endl;

        // cout << "Tabu len: " << this->tabuList.size() << endl;   
    }

    // cout << "iters wo update" <<  iters_without_update << endl;
}
