#include "../include/BBSolver.h"

void BBSolver::solve(arrType map)
{
    this->map = map; // save the map
    min_cost = INT_MAX;

    auto first_visited = vector<int>();
    first_visited.push_back(0);

    // BBSolver::solve_recursion_step(0, 0, nothing_visited);

    for (int i = 1; i < BBSolver::map.size(); i++)
    {
        solve_recursion_step(i, 0, first_visited);
    }

    // std::cout << "BB cost " << BBSolver::best_path << std::endl; 
}



int BBSolver::estimate_lower_bound(const vector<int> &already_visited) {


    int result = *std::min_element(std::begin(map[0]), std::end(map[0]));

    for (int i = 1; i < BBSolver::map.size(); i++) {
        // if the i-th vertice has not yet been visited
        if (std::find(already_visited.begin(), already_visited.end(), i) == already_visited.end())
        {
            // cout << "will i segfault?" << endl;
            result += *std::min_element(std::begin(map[i]), std::end(map[i]));
            // cout << "NOPE" << endl;
        }
    }

    return result;
}


//                                                      copied on each call
void BBSolver::solve_recursion_step(int new_vertice, int current_cost, vector<int> already_visited)
{
    current_cost += map[already_visited.back()][new_vertice];
    already_visited.push_back(new_vertice);


    // upper bound
    if (current_cost > min_cost) {

        // std::cout << "Killing the branch, cost " << current_cost << " > " << best_path << std::endl; 

        return;

    }

    
    int lower_bound = estimate_lower_bound(already_visited);

    if (current_cost + lower_bound > min_cost) {
        return;
    }


    bool anything_found = false;
    for (int i = 1; i < map.size(); i++)
    {

        // if the i-th vertice has not yet been visited
        if (std::find(already_visited.begin(), already_visited.end(), i) == already_visited.end())
        {
            anything_found = true;
            solve_recursion_step(i, current_cost, already_visited);
        } else {
            // std::cout << "Can't move to " << i << ", already visited" << std::endl;
        }
    }

    if(!anything_found) {
        int full_cost = current_cost + map[already_visited.back()][0];
        
        // std::cout << "Moving from " << already_visited.back() << " too " << 0 << ". cost: "  << BBSolver::map[already_visited.back()][0] << std::endl;
        already_visited.push_back(0);


        //std::cout << "SOLVED, full cost: " << full_cost << std::endl;

        // std::cout << "# Path:" << std::endl;
        // for(int i = 1 ; i < already_visited.size(); i++) {
        //     std::cout << already_visited[i-1] << " -> " <<   already_visited[i] << ": " << map[already_visited[i-1]][already_visited[i]] << std::endl;
        // }


        if (full_cost < min_cost) {
            min_cost = full_cost;

        this->best_path = already_visited;
        }
    }
}
