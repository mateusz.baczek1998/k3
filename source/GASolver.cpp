#include "../include/GASolver.h"
#include <algorithm>
#include <random>

using namespace std;

int GASolver::evaluate_speciman(const GASpecimen &specimen)
{
    int cost = 0;

    int for_limit = specimen.genes.size() - 1;

    // cout << "Evaluating a specimen of size " << for_limit+1 << endl;

    for (int i = 0; i < for_limit; i++)
    {
        // cout << "route from " << i << " to " << i+1 << endl;
        // cout << "cities are " << specimen.genes[i] << " -> " << specimen.genes[i+1] << endl;

        cost += this->map[specimen.genes[i]][specimen.genes[i + 1]];
        // cout << "evaluated" << endl;
    }
    // cout << cost << endl;
    return cost;
}

bool GASolver::stoppingCondition()
{
    this->timer.stop();
    return timer.getTimeInMicroSec() > this->max_time_microseconds;
}

void GASolver::generate_new_population()
{
    specimen.clear();

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine rng(seed);

    for (int i = 0; i < this->population_size; i++)
    {
        GASpecimen new_specimen(this->map_size);

        for (int j = 0; j < this->map.size(); j++)
        {
            new_specimen.genes.push_back(j);
        }

        new_specimen.genes.push_back(0);

        std::shuffle(std::begin(new_specimen.genes) + 1, std::end(new_specimen.genes) - 1, rng);

        this->specimen.push_back(new_specimen);
    }
}

void GASolver::evaluate_population()
{

    for (int i = 0; i < this->specimen.size(); i++)
    {
        int result = evaluate_speciman(this->specimen[i]);
        //    cout << result << endl;
        this->specimen[i].result = result;
    }

    // cout << "end of evaluation" << endl;
}

void GASolver::breed_new_population()
{

    // cout << "sorting" << endl;

    // Sortuj od najlepszego do najgorszego
    std::sort(this->specimen.begin(), this->specimen.end(),
              [](GASpecimen const &a, GASpecimen const &b) {
                  return a.result < b.result;
              });

    // cout << "normalisation" << endl;

    auto best_in_generation = this->specimen.front();

    if (best_in_generation.result < this->min_cost)
    {
        // cout << "New, better result:" << this->specimen.front().result << endl;
        this->min_cost = best_in_generation.result;
        this->best_path = best_in_generation.genes;
    }

    vector<GASpecimen> new_population;
    // cout << "reserve 1" << endl;
    // new_population.reserve(population_size);
    // cout << "NOPE" << endl;

    // Elitism
    new_population.push_back(best_in_generation);

    // One from top five
    int min = 0;
    int max = 4;

    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(min, max);

    GASpecimen *a;
    GASpecimen *b;

    while (new_population.size() != this->population_size)
    {
        // Choose first parent
        int random = distr(generator);
        a = &this->specimen[random];

        // Choose second parent
        random = distr(generator);
        // cout << "random " << random << endl;

        b = &this->specimen[random];

        // cout << " new one is being born!" << endl;
        new_population.push_back(
            GASpecimen::breed_from(a, b, this->mutation_chance));
        // cout << "birth done" << endl;
    }

    // cout << "repopulation" << endl;

    this->specimen = new_population;
}

void GASolver::solve(arrType map)
{
    this->min_cost = 1000000;
    this->map = map;
    this->map_size = map.size();

    this->best_path.clear();

    this->timer.start();

    generate_new_population();

    int iter = 1;
    float real_result =  39;
    while (!stoppingCondition())
    {
        // cout << "eval" << endl;
        evaluate_population();
        // cout << "breed" << endl;
        breed_new_population();


        cout << iter++ << ", " << (this->min_cost - real_result)/real_result*100.0 << endl;

    }
}
