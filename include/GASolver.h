#ifndef GA_SOLVER
#define GA_SOLVER

#include "./ResourceMgr.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <bits/stdc++.h>
#include "./solver.h"
#include "./Timer.h"

class GASpecimen
{
public:
    vector<int> genes;
    int result;

    GASpecimen(int size)
    {
        genes.reserve(size);
    }

    void print()
    {
        cout << "{" << endl;
        for (auto gene : this->genes)
        {
            cout << "  " << gene << endl;
        }

        cout << "}" << endl;
    }

    static GASpecimen breed_from(const GASpecimen *a, const GASpecimen *b, const float &mutation_chance)
    {
        // Breeding

        // cout << "null checking" << endl;
        if(a == NULL) {
            cout << "GOT NULL PARENT A" << endl;
        }

        if(b == NULL) {
            cout << "GOT NULL PARENT B" << endl;
        }

        // cout << "parent genom size: ";
        // cout << a->genes.size() << endl;

        GASpecimen child(a->genes.size());

        
        child.genes.push_back(0);
        for (int i = 0; i < a->genes.size()-2; i++)
        {
            child.genes.push_back(-1);
        }
        child.genes.push_back(0);

        int min = 1;
        // int max = a->genes.size() - 1;
        int max = a->genes.size()-2;

        std::random_device rand_dev;
        std::mt19937 generator(rand_dev());
        std::uniform_int_distribution<int> distr(min, max);

        int genes_from_a_start = distr(generator);
        std::uniform_int_distribution<int> distr2(genes_from_a_start, max);
        int genes_from_a_end = distr2(generator);

        // cout << "Copying from " << genes_from_a_start << " to " << genes_from_a_end << endl;

        for (int i = genes_from_a_start; i < genes_from_a_end; i++)
        {
            // cout << i << endl;
            child.genes[i] = a->genes[i];
        }

        // cout << "Done A copying" << endl;
        int b_counter = 1;
        int child_counter = 1;

        while (child_counter != child.genes.size()-1)
        {
            // cout << "child counter " << child_counter << endl;
            // cout << "c gene value  " << child.genes[child_counter] << endl;
            // cout << "b     counter " << b_counter << endl;
            // cout << "b gene value  " << b->genes[b_counter] << endl;

            // Jeśli dany gen ma już wartość od rodzica
            if (child.genes[child_counter] != -1)
            {
                // Idź do kolejnego genu
                child_counter++;
                continue;
            }

            // Jeśli genom dziecka zawiera już dane miasto z rodzica b
            if (std::find(child.genes.begin(), child.genes.end(), b->genes[b_counter]) != child.genes.end())
            {
                // Przejdź do kolejnego genu rodzica
                b_counter++;
                continue;
            }

            // Jeśli genom dziecka ma wolne miejsce i aktualnie rozważane miasto rodzica b
            // nie znajduje się w genomie dziecka
            else
            {
                child.genes[child_counter] = b->genes[b_counter];
                child_counter++;
                b_counter++;
                continue;
            }
        }

        // Mutation
        std::random_device mutation_rand_dev;
        std::mt19937 mutation_generator(rand_dev());
        std::uniform_int_distribution<int> mutation_distr(0, 100);

        
        if (mutation_distr(mutation_generator) < mutation_chance)
        {
            // cout << "yes mutate" << endl;

            std::random_device mutation_location_rand_dev;
            std::mt19937 mutation_location_generator(mutation_location_rand_dev());
            std::uniform_int_distribution<int> mutation_location_distr(1, child.genes.size()-2);

            int swap_1 = mutation_location_distr(mutation_location_generator);
            int swap_2 = mutation_location_distr(mutation_location_generator);

            // cout << "mutation: swapping " << swap_1 << " and " << swap_2 << endl;

            std::swap(
                child.genes[swap_1],
                child.genes[swap_2]
            );

        }

        // cout << "resulting genes" << endl;
        // for(auto gene : child.genes) {
            // cout << gene << endl;
        // }
        // cout << "eog" << endl;

        return child;
    }
};

class GASolver : public Solver
{
public:
    int map_size;

    int max_time_microseconds;
    int population_size;
    vector<GASpecimen> specimen;
    float mutation_chance;

    Timer timer;

    int evaluate_speciman(const GASpecimen &specimen);

    void generate_new_population();
    void evaluate_population();
    void save_best_specimen();
    void breed_new_population();

    void solve(arrType map);
    bool stoppingCondition();
};

#endif