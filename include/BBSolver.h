#ifndef BV_SOLVER
#define BV_SOLVER

#include "./ResourceMgr.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include<bits/stdc++.h> 
#include "./solver.h"

class BBSolver : public Solver
{
public:
    void solve(arrType map);
    
    void solve_recursion_step(int new_vertice, int current_cost, vector<int> already_visited);
    int estimate_lower_bound(const vector<int> &already_visited);
    // void display_result();
};

#endif