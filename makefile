CC = g++
CFlags = -std=c++17 
ReleaseFlags = -O3
Sources =  $(wildcard source/*.cpp)
TRASH = $(wildcard *.gch) $(wildcard */*.gch)
RUN = && ./a.out

all:
		$(CC) $(CFlags) $(Sources)

clear:
		rm -rf $(TRASH)

debug:

		echo "Kompilowaie"

release:
		$(CC) $(CFlags) $(Sources) $(ReleaseFlags)
